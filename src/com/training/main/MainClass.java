package com.training.main;

import com.training.bill.BillGenerator;

public class MainClass {

	public static void main(String[] args) {
		BillGenerator billGenerator = new BillGenerator();
		billGenerator.generateBill();
	}
}