package com.training.bill;

import java.util.Scanner;

public class BillGenerator {

	private int units;
	private double bill;
	private static final double CGST = 5.0;
	private static final double SGST = 4.0;
	private double cgstAmount;
	private double sgstAmount;

	public void generateBill() {
		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.print("Enter valid consumed units : ");
			units = scanner.nextInt();
			if (units > 0) {
				scanner.close();
				billCalculator();
				break;
			}
		}
	}

	private void billCalculator() {
		if (units >= 1 && units <= 50) {
			double netBill = units * .5;
			cgstAmount = CGST * netBill / 100;
			sgstAmount = SGST * netBill / 100;
			bill = netBill + cgstAmount + sgstAmount;
		} else if (units >= 51 && units <= 150) {
			double netBill = (50 * .5) + ((units - 50) * .75);
			cgstAmount = CGST * netBill / 100;
			sgstAmount = SGST * netBill / 100;
			bill = netBill + cgstAmount + sgstAmount;
		} else if (units >= 151 && units <= 250) {
			double netBill = (50 * .5) + (100 * .75) + ((units - 150) * 1.25);
			cgstAmount = CGST * netBill / 100;
			sgstAmount = SGST * netBill / 100;
			bill = netBill + cgstAmount + sgstAmount;
		} else {
			double netBill = (50 * .5) + (100 * .75) + (100 * 1.25) + ((units - 250) * 1.5);
			cgstAmount = CGST * netBill / 100;
			sgstAmount = SGST * netBill / 100;
			bill = netBill + cgstAmount + sgstAmount;
		}

		display();
	}

	private void display() {
		System.out.println("UNITS : " + units);
		System.out.println("CGST : " + cgstAmount);
		System.out.println("SGST : " + sgstAmount);
		System.out.println("TOTAL BILL : " + bill);
	}
}